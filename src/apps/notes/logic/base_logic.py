from fastapi import HTTPException
from sqlalchemy.orm import Session

from src.apps.notes import crud, schemas


def create_note(db: Session, note: schemas.NoteCreate):
    db_note = crud.get_note_by_name(db, name=note.name)
    if db_note:
        raise HTTPException(status_code=400, detail="Note already exists")
    return crud.create_note(db=db, note=note)


def notes_list(db: Session, page: int, page_size: int):
    return crud.get_notes(db=db, page=page, page_size=page_size)


def get_note(db: Session, id: int):
    db_note = crud.get_note(db=db, id=id)
    if not db_note:
        raise HTTPException(status_code=404)
    return db_note


def update_note(db: Session, note: schemas.NoteCreate, id: int):
    return crud.update_note(db=db, id=id, note=note)


def delete_note(db: Session, id: int):
    return crud.delete_note(db=db, id=id)


def delete_all_notes(db: Session):
    return crud.delete_all_notes(db=db)
