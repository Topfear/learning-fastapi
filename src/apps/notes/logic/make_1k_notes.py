import random  
import string

from sqlalchemy.orm import Session

from src.apps.notes import models, schemas  


def make_random_string(length): 
    symbols = string.ascii_lowercase + '.' + ' '
    text = ''.join((random.choice(symbols) for x in range(length)))
    cap_text = string.capwords(text, sep='.')
    return cap_text


async def make_1k_notes(db: Session):
    for _ in range(1000):
        name = make_random_string(10)
        description = make_random_string(50)
        note = schemas.NoteCreate(name=name, description=description)
        db_note = models.Note(name=note.name, description=note.description)
        db.add(db_note)
    db.commit()
