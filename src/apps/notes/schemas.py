from typing import Optional

from pydantic import BaseModel


class NoteBase(BaseModel):
    name: str
    description: Optional[str] = ''


class NoteCreate(NoteBase):
    pass


class Note(NoteBase):
    id: int

    class Config:
        orm_mode = True
