from sqlalchemy import Column, Integer, String

from src.core.database import Base


class Note(Base):
    __tablename__ = "note"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String(20), index=True)
    description = Column(String(100))
