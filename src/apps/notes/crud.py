from sqlalchemy import update
from sqlalchemy.orm import Session
from fastapi.encoders import jsonable_encoder

from . import models, schemas


def get_note(db: Session, id: int):
    return db.query(models.Note).filter(models.Note.id == id).first()


def get_note_by_name(db: Session, name: str):
    return db.query(models.Note).filter(models.Note.name == name).first()


def get_notes(db: Session, page: int = 1, page_size: int = 100):
    limit = page_size
    offset = page_size * (page - 1)
    return db.query(models.Note).offset(offset).limit(limit).all()


def create_note(db: Session, note: schemas.NoteCreate):
    db_note = models.Note(name=note.name, description=note.description)
    db.add(db_note)
    db.commit()
    return db_note


def delete_note(db: Session, id: int):
    db_note = get_note(db=db, id=id)
    db.delete(db_note)
    db.commit()


def delete_all_notes(db: Session):
    db.query(models.Note).delete()
    db.commit()


def update_note(db: Session, note: schemas.NoteCreate, id: int):
    note = jsonable_encoder(note)

    db.query(models.Note).\
       filter(models.Note.id == id).\
       update(note)
    db.commit()
       
    db_note = get_note(db=db, id=id)
    return db_note
