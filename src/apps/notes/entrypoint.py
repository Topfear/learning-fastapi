from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

from src.core.dependency import get_db
from .logic import base_logic
from .logic.make_1k_notes import make_1k_notes
from . import schemas

notes_router = APIRouter()


@notes_router.post(
    "/", tags=['Notes'], 
    summary="Создание заметки",
    response_model=schemas.Note)
def post_note(note: schemas.NoteCreate, db: Session = Depends(get_db)):
    return base_logic.create_note(db=db, note=note)


@notes_router.get(
    "/", tags=['Notes'], 
    summary="Постраничное чтение заметок",
    response_model=list[schemas.Note])
def get_notes(db: Session = Depends(get_db), page: int = 1, page_size: int = 100):
    return base_logic.notes_list(db=db, page=page, page_size=page_size)


@notes_router.delete(
    "/", tags=['Notes'], 
    summary="Удаление всех заметок",
    status_code=204)
def delete_all_notes(db: Session = Depends(get_db)):
    return base_logic.delete_all_notes(db=db)


@notes_router.get(
    "/{id}/", tags=['Notes'], 
    summary="Получение заметки по id",
    response_model=schemas.Note)
def get_note_by_id(id: int, db: Session = Depends(get_db)):
    return base_logic.get_note(db=db, id=id)


@notes_router.put(
    "/{id}/", tags=['Notes'],
    summary="Изменение заметки по id",
    response_model=schemas.Note)
async def update_item(id: int, note: schemas.NoteCreate, db: Session = Depends(get_db)):
    return base_logic.update_note(db=db, id=id, note=note)


@notes_router.delete(
    "/{id}/", tags=['Notes'], 
    summary="Удаление заметки по id",
    status_code=204)
def delete_note_by_id(id: int, db: Session = Depends(get_db)):
    return base_logic.get_note(db=db, id=id)


@notes_router.post(
    "/create_1k_notes/", tags=['Notes'], 
    summary="Создание 1к заметок",
    status_code=200)
async def generate_1k_notes(db: Session = Depends(get_db)):
    return await make_1k_notes(db=db)
