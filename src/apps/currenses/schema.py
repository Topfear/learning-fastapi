from uuid import UUID
from pydantic import BaseModel


class CurrencySchema(BaseModel):
    USD_RUB: str
    EUR_RUB: str
    CNY_RUB: str
    AZN_RUB: str
    BTC_RUB: str
    BYN_RUB: str


class UuidSchema(BaseModel):
    uuid: UUID
