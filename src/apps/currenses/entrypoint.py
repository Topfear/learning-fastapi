from typing import List
from fastapi import APIRouter, Request

from .schema import CurrencySchema, UuidSchema
from .logic import currency_now, test_async_requests

currenses_router = APIRouter()


@currenses_router.get("/now/", 
    tags=['Сurrency'], 
    response_model=CurrencySchema,
    summary="Стоимость валюты в рублях на данный момент")
async def get_currency(request: Request):
    """
    Валюты:
    - **USD_RUB**: стоимость доллара в рублях
    - **EUR_RUB**: стоимость евро в рублях
    - **CNY_RUB**: стоимость китайского юаня в рублях
    - **AZN_RUB**: стоимость азербайджана в рублях
    - **BTC_RUB**: стоимость биткоина в рублях
    - **BYN_RUB**: стоимость белорусского рубля в рублях
    """
    return await currency_now(request)


@currenses_router.get("/test/", 
    tags=['Сurrency'], 
    summary="Тестовая ручка",
    response_model=List[UuidSchema])
async def get_currency(request: Request):
    """
    Ручка возвращает 100 uid'ов из внешнего API
    """
    return await test_async_requests(request)
