import asyncio
from fastapi import HTTPException
import httpx


async def fetch(client, url):
    response = await client.get(url)
    return response


async def fetch_all(urls, merge_results=False):
    async with httpx.AsyncClient() as client:
        tasks = [fetch(client, url) for url in urls]
        results = await asyncio.gather(*tasks)

        # Если один из запросов провалился, то возвращаем ошибку
        failed = map(lambda x: httpx.codes.is_error(x.status_code), results)
        if any(failed):
            raise HTTPException(
                status_code=results[0].status_code, 
                detail=f"service unaviable for reason: {results[0].status_code}")

        # Соединение результатов в один словарь
        if merge_results:
            merged_result = {}
            for result in results:
                merged_result.update(result.json())
            return merged_result

        results = [x.json() for x in results]
        return results
