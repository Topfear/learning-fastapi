import logging
from time import time

from fastapi import Request

from src.core.settings import currencyconverterapi_token
from .utils import fetch_all

logger = logging.getLogger(__name__)

class CurrencyConverterUrls:
    url = (
        "https://free.currconv.com/"
        "api/v7/convert"
        "?q={}"
        "&compact=ultra"
        f"&apiKey={currencyconverterapi_token}"
    )
    
    usd_eur_convert_list = ",".join([
        "USD_RUB",      # США
        "EUR_RUB",      # Евро
    ])
    china_azer_convert_list = ",".join([
        "CNY_RUB",      # Китай
        "AZN_RUB",      # Азербайджан
    ])
    btc_belor_convert_list = ",".join([
        "BTC_RUB",      # Биткоин
        "BYN_RUB",      # Белорусь
    ])
    
    url_USD_EUR = url.format(usd_eur_convert_list)
    url_CHINA_AZER = url.format(china_azer_convert_list)
    url_BTC_BELOR = url.format(btc_belor_convert_list)


async def currency_now(request: Request):
    urls = [
        CurrencyConverterUrls.url_USD_EUR, 
        CurrencyConverterUrls.url_CHINA_AZER, 
        CurrencyConverterUrls.url_BTC_BELOR
    ]

    start = time()
    result = await fetch_all(urls, merge_results=True)
    end = time()
    request.app.logger.info(f'Async requests took: {(end - start) * 1000:.0f}ms')
    return result


async def test_async_requests(request: Request):
    urls = ["http://httpbin.org/uuid"] * 100

    start = time()
    result = await fetch_all(urls, merge_results=False)
    end = time()
    request.app.logger.info(f'Async requests took: {(end - start) * 1000:.0f}ms')
    return result
