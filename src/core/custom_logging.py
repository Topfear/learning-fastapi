import logging
import sys
from pathlib import Path
from loguru import logger


class InterceptHandler(logging.Handler):
    loglevel_mapping = {
        50: 'CRITICAL',
        40: 'ERROR',
        30: 'WARNING',
        20: 'INFO',
        10: 'DEBUG',
        0: 'NOTSET',
    }

    def emit(self, record):
        try:
            level = logger.level(record.levelname).name
        except AttributeError:
            level = self.loglevel_mapping[record.levelno]

        frame, depth = logging.currentframe(), 2
        while frame.f_code.co_filename == logging.__file__:
            frame = frame.f_back
            depth += 1

        log = logger.bind()
        log.opt(
            depth=depth,
            exception=record.exc_info
        ).log(level,record.getMessage())


class CustomizeLogger:

    @classmethod
    def make_logger(cls, logger_settings: object):
        logger = cls.customize_logging(
            filepath=getattr(logger_settings, 'path', None),
            level=getattr(logger_settings, 'level', 'info'),
            retention=getattr(logger_settings, 'retention', None),
            rotation=getattr(logger_settings, 'rotation', None),
            errors_path=getattr(logger_settings, 'errors_path', None),
        )
        return logger

    @classmethod
    def customize_logging(cls,
            filepath: Path,
            level: str,
            rotation: str,
            retention: str,
            errors_path: str = None,
    ):

        logger.remove()

        # Консольный логгер
        logger.add(
            sys.stdout,
            enqueue=True,
            backtrace=True,
            level=level.upper()
        )
        # Логи сохраняются в файл
        if filepath:
            logger.add(
                str(filepath),
                rotation=rotation,
                retention=retention,
                enqueue=True,
                backtrace=True,
                level=level.upper()
            )
        # Ошибки сохраняются в файл
        if errors_path:
            logger.add(
                str(errors_path),
                rotation=rotation,
                retention=retention,
                enqueue=True,
                backtrace=True,
                level="ERROR"
            )

        logging.basicConfig(handlers=[InterceptHandler()], level=0)
        logging.getLogger("uvicorn.access").handlers = [InterceptHandler()]
        for _log in ['uvicorn',
                     'uvicorn.error',
                     'fastapi'
                     ]:
            _logger = logging.getLogger(_log)
            _logger.handlers = [InterceptHandler()]

        return logger.bind()
