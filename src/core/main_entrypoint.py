from fastapi import APIRouter

from src.apps.currenses.entrypoint import currenses_router
from src.apps.notes.entrypoint import notes_router


router = APIRouter()
router.include_router(currenses_router, prefix="/currency")
router.include_router(notes_router, prefix="/notes")
