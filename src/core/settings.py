from dotenv import dotenv_values

config = {
    **dotenv_values(".env.shared"),  # загружает общие переменные
    **dotenv_values(".env.secret"),  # загружает секретные переменные
    # **os.environ,                  # загружает переменные из окружения
}

# Префикс для API
BASE_API_URL = config['BASE_API_URL']

# Хост и порт
SERVER_HOST = config['SERVER_HOST']
SERVER_PORT = int(config['SERVER_PORT'])

# Настройки отладки
DEBUG = bool(config['DEBUG'])

# Токен для внешнего API (надо спрятать)
currencyconverterapi_token = config['DEBUG']

# настройки логгирования
class LoggerSettings:
    path: str =  "./logs/access.log"
    errors_path: str = "./logs/error.log"
    level: str = "info"
    rotation: str = "00:00"             # Новый лог файл каждый день в полночь
    retention: str = "10 days"          # Сохраняет логи в течении 10 дней
