"""create_notes_table

Revision ID: a21f2e28fee8
Revises: 
Create Date: 2022-04-19 09:24:26.605041

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'a21f2e28fee8'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'note',
        sa.Column('id', sa.Integer, primary_key=True, index=True),
        sa.Column('name', sa.String(20), index=True),
        sa.Column('description', sa.String(100)),
    )


def downgrade():
    op.drop_table('note')
