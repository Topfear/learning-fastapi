from distutils.log import debug
import uvicorn
from datetime import datetime

from fastapi import FastAPI
from src.core.custom_logging import CustomizeLogger

from src.core.settings import BASE_API_URL, SERVER_HOST, SERVER_PORT, LoggerSettings, DEBUG
from src.core.main_entrypoint import router


def create_app():
    app = FastAPI()
    logger = CustomizeLogger.make_logger(LoggerSettings)
    app.logger = logger
    app.include_router(router=router, prefix=BASE_API_URL)
    return app

app = create_app()


if __name__ == "__main__":
    app.logger.info(f"🚀 App started at {datetime.now().strftime('%H:%M %d.%m.%Y')}! 🚀")
    uvicorn.run("main:app",
                host=SERVER_HOST,
                port=SERVER_PORT,
                debug=DEBUG,
                reload=True)
    app.logger.info(f"🏁 App finished at {datetime.now().strftime('%H:%M %d.%m.%Y')}! 🏁")
